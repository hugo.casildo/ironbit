const express = require("express");
const cors = require("cors");
const app = express();
const Services = require("./src/services");
const service = new Services()

app.use(cors());

app.get("/prime/:number", (req, res) => {
  console.log(service.getPrimes(req.params.number));
  res.json(service.getMultiples(req.params.number));
});

app.get("/multiples/:number", (req, res) => {
  console.log(service.getMultiples(req.params.number));
  res.json(service.getMultiples(req.params.number));
});

app.listen(3000);
