const Services  = require('../services');
test("should return 10 first multiples of 3", () => {
  const services = new Services();
  expect(services.getMultiples(10)).toEqual([
    3, 6, 9, 12, 15, 18, 21, 24, 27, 30,
  ]);
});

test("should return 10 first primes", () => {
  const services = new Services();
  expect(services.getPrimes(10)).toEqual([2, 3, 5, 7, 11, 13, 17, 19, 23, 29]);
});
