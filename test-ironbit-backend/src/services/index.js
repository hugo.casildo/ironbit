module.exports = class Services {
  constructor() {}

  getMultiples = (n) => {
    const multiples = [];
    for (let i = 1; multiples.length < n; i++) {
      multiples.push(3 * i);
    }
    return multiples;
  };

  getPrimes = (n) => {
    const primes = [];
    let i = 2;

    while (primes.length < n) {
      if (this.isPrime(i)) {
        primes.push(i);
      }
      i++;
    }
    return primes;
  };

  isPrime = (n) => {
    if (n < 2) return false;
    if (n == 2) return true;
    if (n % 2 == 0) return false;

    let maxDiv = Math.sqrt(n);
    for (let i = 3; i <= maxDiv; i += 2) {
      if (n % i == 0) {
        return false;
      }
    }
    return true;
  };
};
