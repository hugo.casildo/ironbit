const URL = 'http://localhost:3000/'
const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json'
}

function getPrimes(payload) {
  return fetch(`${URL}prime/${payload}`, {
    mode: 'cors',
    headers
  }).then((resp) => resp.json())
}

function getMultiples(payload) {
  return fetch(`${URL}multiples/${payload}`, {
    method: 'GET',
    mode: 'cors',
    headers
  }).then((resp) => resp.json())
}


export default {
  getPrimes,
  getMultiples
}