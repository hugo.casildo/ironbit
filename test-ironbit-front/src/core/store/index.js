import { createStore } from 'vuex'
import api from '@/core/api'

export const store = createStore({
  state: {
    number: 5,
    prime: [],
    sequence: []
  },
  mutations: {
    updateNumber: (state, payload) => state.number = payload,
    updatePrime: (state, payload) => state.prime = payload,
    updateSequence: (state, payload) => state.sequence = payload
  },
  getters: {
    getNumber: (state) => state.number,
    getPrime: (state) => state.prime,
    getSequence: (state) => state.sequence
  },
  actions: {
    async updateNumberAction({ commit }, payload) {
      const primes = await api.getPrimes(payload)
      const multiples = await api.getMultiples(payload)

      commit('updatePrime', primes)
      commit('updateSequence', multiples)
    }
  }
})
