# Prueba de Front end Ironbit - Hugo Casildo

## Instalación

### Backend

Para instalar el backend es necesario navegar hasta el directorio `test-ironbit-backend`

```
cd test-ironbit-backend
npm install
```

### Front end

Para instalar el frontend es necesario navegar hasta el directorio `test-ironbit-front`

```
cd test-ironbit-front
npm install

```

## Iniciar servidores
Es necesario iniciar el sevidor de backend antes que el servidor de front 
### Backend
Para iniciar el servidor de backend solo es necesario ejecutar el siguiente comando
```
npm start
```

### Front end
Para iniciar el servidor Front end solo es necesario ejecutar el siguiente comando
```
npm run dev
```

El servidor se levantará en el puerto [5173](http://localhost:5173), de estar ocupado dicho puerto, puede levantarse en el puerto consecutivo disponible
